from django.test import TestCase, Client
from django.urls import reverse
from api_demanda.models import DemandaPeca
import json
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient

class TestViews(TestCase):

	def setUp(self):
		self.client = APIClient()
		self.user = get_user_model().objects.create_user(
			'AnuncianteTeste', 'droidfinxi')
		self.client.force_authenticate(self.user)

		DemandaPeca.objects.create(descricao_peca = 'teste_descricao',
			endereco_entrega = 'teste_endereco', contato = 'teste_contato',
			anunciante = 'teste_anunciante', status = 'baseline-highlight_off.svg')

	def test_demanda_list_GET(self):
		response = self.client.get(reverse('demanda_list'))
		self.assertEquals(response.status_code, 200)

	
	def test_demanda_detail_GET(self):
		response = self.client.get(reverse('demanda_detail', kwargs={'pk':1}))
		self.assertEquals(response.status_code, 200)
	
	def test_demanda_detail_PUT(self):
		response = self.client.put(reverse('demanda_detail', kwargs={'pk':1}), format='json')
		self.assertEquals(response.status_code, 200)
	
	def test_demanda_detail_DELETE(self):
		response = self.client.delete(reverse('demanda_detail', kwargs={'pk':1}))
		self.assertEquals(response.status_code, 200)
	
	def test_demanda_detail_POST_no_data(self):
		demanda = {}
		demanda_str = json.dumps(demanda)
		demanda_json = json.loads(demanda_str)
		
		response = self.client.post(reverse('demanda_list'), demanda_json, format='json')
		self.assertEquals(response.status_code, 400)
	
	def test_demanda_detail_POST(self):
		demanda = { 'id': 10, 'descricao_peca': 'teste', 'endereco_entrega': 'teste', 'contato': 'teste', 'anunciante': 'Administrador',
			'status' : 'baseline-highlight_off.svg' }
		
		demanda_str = json.dumps(demanda)
		demanda_json = json.loads(demanda_str)
		response = self.client.post(reverse('demanda_list'), demanda_json, format='json')
		
		#self.assertEquals(response.status_code, 201)