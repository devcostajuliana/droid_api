from django.test import SimpleTestCase
from django.urls import reverse, resolve
from api_demanda.views import demanda_list, demanda_detail

class TestUrls(SimpleTestCase):

	def test_demanda_list_url_is_resolved(self):
		url = reverse('demanda_list')
		print(resolve(url))
		self.assertEquals(resolve(url).func, demanda_list)
	
	def test_demanda_detail_url_is_resolved(self):
		url = reverse('demanda_detail', kwargs={'pk':1})
		print(resolve(url))
		self.assertEquals(resolve(url).func, demanda_detail)
