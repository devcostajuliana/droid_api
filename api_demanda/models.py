from django.db import models

# Create your models here.

def upload_location(instance, filename, **kwargs):
	file_path = '{filename}'.format(
			filename=filename
		)
	return file_path

class DemandaPeca(models.Model):
	descricao_peca = models.CharField(max_length=100)
	endereco_entrega = models.CharField(max_length=100)
	contato = models.CharField(max_length=100)
	anunciante = models.CharField(max_length=100)
	status =  models.FileField(upload_to=upload_location, null=False, blank=False)
	
	def __str__(self):
		return self.descricao_peca
