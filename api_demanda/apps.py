from django.apps import AppConfig


class ApiDemandaConfig(AppConfig):
    name = 'api_demanda'
