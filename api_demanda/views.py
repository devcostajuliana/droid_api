from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.parsers import JSONParser
from .models import DemandaPeca
from .serializers import DemandaPecaSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, permission_classes

from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework import mixins
from rest_framework.authentication import SessionAuthentication, TokenAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated,))
def demanda_list(request):
	
	account = request.user
	
	if request.method == 'GET':
		if str(account) != "Administrador":
			pecas = DemandaPeca.objects.filter(anunciante=account)
		else:
			pecas = DemandaPeca.objects.all()	
		serializer = DemandaPecaSerializer(pecas, many=True)
		
		return Response(serializer.data)
		
	if request.method == 'POST':
		request.data.update({'anunciante': str(account)})
		
		serializer = DemandaPecaSerializer(data=request.data)
		
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		
		

#@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((IsAuthenticated,))
def demanda_detail(request, pk):
	try:
		peca = DemandaPeca.objects.get(pk=pk)

	except DemandaPeca.DoesNotExist:
		return HttpResponse(status=status.HTTP_404_NOT_FOUND)

	user = request.user
	
	if str(peca.anunciante) != str(user) and str(user) != 'Administrador':
		return Response({'response': "You do not have permission to edit that."})
		
	if request.method == 'GET':
		serializer = DemandaPecaSerializer(peca)
		return Response(serializer.data)
	
	elif request.method == 'PUT':
		request.data.update({'anunciante': str(user)})
		serializer = DemandaPecaSerializer(peca, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		peca.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)