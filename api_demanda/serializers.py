from rest_framework import serializers
from .models import DemandaPeca

class DemandaPecaSerializer(serializers.ModelSerializer):
		
	class Meta:
		model = DemandaPeca
		fields = ['id', 'descricao_peca', 'endereco_entrega', 'contato', 'status', 'anunciante']
		#fields = '__all__'