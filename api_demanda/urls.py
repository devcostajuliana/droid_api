from django.urls import path
from .views import demanda_list, demanda_detail
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('demanda/', demanda_list, name='demanda_list'),
    path('detail/<int:pk>/', demanda_detail, name='demanda_detail'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

